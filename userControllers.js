

const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//registerUser is a method and an anonymous function in an object
module.exports.registerUser = (req,res)=>{

    const hashedPw = bcrypt.hashSync(req.body.password,10);
    console.log(hashedPw);

    let newUser = new User({

        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPw,
        mobileNo: req.body.mobileNo
    })

    newUser.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

module.exports.getUserDetails = (req,res)=>{

    console.log(req.user)

    //find() will return an array of documents which matches the criteria.
    //It will return an array even if it only found 1 document.
    //User.find({_id:req.body.id})/

    //findOne() will return a single document that matched our criteria.
    //User.findOne({_id:req.body.id})

    //findById() is a mongoose method that will allow us to find a document strictly by its id.

    // User.findById(req.body.id)

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(error => res.send(error))

}
module.exports.loginUser = (req,res) => {

    console.log(req.body);

    /*
    steps for loggin in our user:

    1.find the user by its email
    2. If we found the user, we will check his password and the hashed password in our db matches.
    3. If we don't find a user, we will send a message to the client.
    4. If upon checking the found user's password is the same our input password, we will generate a "key" for the user to have authorixation to access certain feautures in our app.
    */



User.findOne({email:req.body.email})
.then(foundUser => {

    if(foundUser === null){
        return res.send({message: "No User Found."})
    } else {
        // console.log(foundUser)

        //check if the input password from req.body matches password in our foundUser document:

        //will return true

        const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

        // console.log(isPasswordCorrect);

        //If the pw is correct, we will create a KEY, a token for our user, else we will send a message:

        if(isPasswordCorrect){

            /*
            to create  a KEY:
            we have to create our own module called auth.js.

            This module will create a encoded string which contains our users details

            this encoded string is what we call a JSONWebtoken or JWT.

            This JWT can oly properly unwrapped or decoded w/ our own secret word/string.
                

            */


            // console.log("We will create a token for the user if the password is correct")

            return res.send({accessToken: auth.createAccessToken(foundUser)});

        } else {

            return res.send({message: "Incorrect Password"});
        }

    }
})


}



// s36-37 activity
module.exports.getEmailDetails = (req,res)=>{
    

    User.findOne({email:req.user.email})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}










